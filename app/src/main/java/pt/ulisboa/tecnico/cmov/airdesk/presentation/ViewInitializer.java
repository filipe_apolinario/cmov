package pt.ulisboa.tecnico.cmov.airdesk.presentation;

/**
 * Created by Group 7 on 02-Apr-15.
 */

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * The Class ViewInitializer.
 */
public class ViewInitializer {

    /**
     * Initialize my workspaces list view.
     *
     * @param listView            the list view
     * @param adapter             the adapter
     * @param onItemClickListener the on item click listener
     */
    public void initializeMyWorkspacesListView(ListView listView,
                                               ArrayAdapter<String> adapter,
                                               AdapterView.OnItemClickListener onItemClickListener) {
        //specify list view behaviour
        //create interaction when list item is pressed
        if (onItemClickListener != null) {
            listView.setOnItemClickListener(onItemClickListener);
        }
        //set listview content
        listView.setAdapter(adapter);
    }
}
