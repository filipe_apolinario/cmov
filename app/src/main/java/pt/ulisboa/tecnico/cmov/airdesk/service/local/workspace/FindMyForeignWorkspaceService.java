package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;

/**
 * The Class FindMyForeignWorkspaceService.
 */
public class FindMyForeignWorkspaceService {

    /**
     * The data source.
     */
    private WorkspaceAccessDataSource dataSource;

    /**
     * The workspace accesses list.
     */
    private ArrayList<WorkspaceAccess> workspaceAccessesList;

    /**
     * Instantiates a new find my foreign workspace service.
     *
     * @param context the context
     */
    public FindMyForeignWorkspaceService(Context context) {
        this.dataSource = new WorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param foreignEmail the email
     */
    public void run(String foreignEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {foreignEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        dataSource.open();

        workspaceAccessesList = dataSource.findForeignPublicWorkspaces(foreignEmail);

        dataSource.close();

    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public ArrayList<WorkspaceAccess> getResult() {
        return workspaceAccessesList;
    }

}
