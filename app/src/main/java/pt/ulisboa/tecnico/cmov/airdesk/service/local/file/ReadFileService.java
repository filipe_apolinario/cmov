package pt.ulisboa.tecnico.cmov.airdesk.service.local.file;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;

/**
 * The Class ReadFileService.
 * This service read file from FileStorage.
 */
public class ReadFileService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * The file content.
     */
    private String fileContent;

    /**
     * Instantiates a new read file service.
     *
     * @param context the context
     */
    public ReadFileService(Context context) {
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param workspaceRelativePath the workspace relative path
     * @param fileName              the file name
     */
    public void run(String workspaceRelativePath, String fileName) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {workspaceRelativePath, fileName};
        if (!verifier.stringVerifier(sParams)) {
            throw new EmptyFieldsException();
        }

        String absolutePath = fileDirectory.getAbsolutePath();

        FileStorageHandler handler = new FileStorageHandler();
        String wsAbsPath = absolutePath + File.separator + workspaceRelativePath;
        if (!handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }

        this.fileContent = handler.read(wsAbsPath + File.separator + fileName);
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public String getResult() {
        return this.fileContent;
    }
}
