package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.remote;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.ReadFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands.ReadFileCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class ReadFileAsyncTask.
 * Fetches the content of the file.
 */
public class ReadFileAsyncTask extends
        AsyncTask<P2PNode, String, String> {

    /**
     * The file name.
     */
    private String fileName;
    /**
     * The Workspace name.
     */
    private String workspaceName;
    /**
     * The owner email.
     */
    private String ownerEmail;


    /**
     * The service.
     */
    private ReadFileService service;

    /**
     * The read file content text.
     */
    private TextView readFileContentText;

    /**
     * The context.
     */
    private Context context;


    /**
     * Instantiates a new read file async task.
     *
     * @param c        the c
     * @param fileName the file name
     * @param textView the text view
     */
    public ReadFileAsyncTask(Context c,
                             String fileName,
                             String workspaceName,
                             String ownerEmail,
                             TextView textView) {
        super();

        this.service = new ReadFileService(c);
        this.fileName = fileName;
        this.workspaceName = workspaceName;
        this.ownerEmail = ownerEmail;
        this.readFileContentText = textView;
        this.context = c;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Reading file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected String doInBackground(P2PNode... params) {
        ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    ReadFileCommand command = new ReadFileCommand(workspaceName, fileName);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();


                    ReadFileCommand response = (ReadFileCommand) service.getResult();
                    publishProgress("Read file successfully");
                    return response.getResult();
                } catch (CommunicationException e) {
                    publishProgress("Could not read file");
                }
            }
        }
        return "";
    }


    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }


    /**
     * On post execute.
     *
     * @param result the result
     */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        this.readFileContentText.setText(result);
    }
}
