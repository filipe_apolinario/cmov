package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.ExchangeEmailCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class RequestEmailTask extends AsyncTask<P2PNode, String, String> {

    public static final String TAG = "simplechat";
    private final ExchangeEmailCommand request;
    SimWifiP2pSocket s;
    private ExchangeEmailCommand response;
    private Context context;

    public RequestEmailTask(Context context, ExchangeEmailCommand request) {
        this.context = context;
        this.request = request;
    }


    @Override
    protected String doInBackground(P2PNode... params) {
        try {
            P2PNode peer = params[0];
            //SimWifiP2pSocket peerSocket = peerSocketNode.getSocket();

            //send request to peer
            publishProgress("Sending request to peer");

            RequestCommunicationService service = new RequestCommunicationService(request, peer, context);
            service.execute();

            this.response = (ExchangeEmailCommand) service.getResult();
            peer.setOwnerEmail(response.getExecuterEmail());

            return "device " + response.getExecuterDeviceName() + " is logged in as " + response.getExecuterEmail();
        } catch (CommunicationException e) {
            return "could not perform request";
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Toast toast = Toast.makeText(context, values[0], Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            Toast toast = Toast.makeText(context, result, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
