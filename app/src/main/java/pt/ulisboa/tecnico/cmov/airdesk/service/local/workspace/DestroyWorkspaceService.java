package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;

/**
 * The Class DestroyWorkspaceService.
 */
public class DestroyWorkspaceService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * The data source access handler.
     */
    private WorkspaceAccessDataSource dataSourceAccessHandler;

    /**
     * The context.
     */
    private Context context;


    /**
     * Instantiates a new destroy workspace service.
     *
     * @param context the context
     */
    public DestroyWorkspaceService(Context context) {
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
        dataSourceAccessHandler = new WorkspaceAccessDataSource(context);
        this.context = context;
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param workspaceName the workspace name
     * @param ownerEmail    the owner email
     */
    public void run(String workspaceName, String ownerEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] params = {workspaceName, ownerEmail};
        if (!verifier.stringVerifier(params)) {
            throw new EmptyFieldsException();
        }

        FetchWorkspaceService service = new FetchWorkspaceService(context);
        service.run(ownerEmail, workspaceName);
        Workspace ws = service.getResult();

        if (ws == null) {
            throw new WorkspaceDoesNotExistException();
        }

        removeFromDatabase(ws);

        deleteDirectoryAndFiles(ws);
    }

    /**
     * Removes the from database.
     *
     * @param ws the ws
     */
    private void removeFromDatabase(Workspace ws) {
        databaseMyWorkspaceHandler.open();

        databaseMyWorkspaceHandler.deleteWorkspace(ws);

        databaseMyWorkspaceHandler.close();

        dataSourceAccessHandler.open();

        dataSourceAccessHandler.removeWorkspaceAccess(ws.getName(), ws.getOwnerEmail());

        dataSourceAccessHandler.close();

    }

    /**
     * Delete directory and files.
     *
     * @param ws the ws
     */
    private void deleteDirectoryAndFiles(Workspace ws) {

        String absolutePath = fileDirectory.getAbsolutePath();
        String wsRelativePath = ws.getDirectory();

        FileStorageHandler handler = new FileStorageHandler();
        String wsAbsPath = absolutePath + File.separator + wsRelativePath;
        if (handler.doesDirectoryExist(wsAbsPath)) {
            handler.removeDirectory(wsAbsPath);
        }
    }
}
