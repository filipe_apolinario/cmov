package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.remote;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.WriteFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands.EditFileCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class EditFileAsyncTask.
 * This task updates the content of the file to the FileStorage
 */
public class EditFileAsyncTask extends
        AsyncTask<P2PNode, String, Void> {

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The Workspace Name.
     */
    private String workspaceName;

    /**
     * The Owner Email.
     */
    private String ownerEmail;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The service.
     */
    private WriteFileService service;

    /**
     * The text.
     */
    private String text;

    /**
     * The quota.
     */
    private int quota;

    /**
     * The context.
     */
    private Context context;


    /**
     * Instantiates a new edits the file async task.
     *
     * @param c          the c
     * @param activity   the activity
     * @param ownerEmail the folder absolute path
     * @param fileName   the file name
     * @param text       the text
     * @param quota      the quota
     */
    public EditFileAsyncTask(Context c,
                             Activity activity,
                             String ownerEmail,
                             String workspaceName,
                             String fileName,
                             String text,
                             int quota) {
        super();

        this.service = new WriteFileService(c);
        this.fileName = fileName;
        this.ownerEmail = ownerEmail;
        this.workspaceName = workspaceName;
        this.activity = activity;
        this.text = text;
        this.quota = quota;
        this.context = c;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Saving file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(P2PNode... params) {
        ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    EditFileCommand command = new EditFileCommand(workspaceName, fileName, text);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();


                    EditFileCommand response = (EditFileCommand) service.getResult();
                    publishProgress(response.getResult());

                } catch (CommunicationException e) {
                    publishProgress("Could not delete file");
                }
            }
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }


    /**
     * On post execute.
     *
     * @param result the result
     */
    @Override
    protected void onPostExecute(Void result) {

        activity.finish();
    }
}
