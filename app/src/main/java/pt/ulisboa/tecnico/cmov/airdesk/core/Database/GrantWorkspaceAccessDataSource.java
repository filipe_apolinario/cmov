package pt.ulisboa.tecnico.cmov.airdesk.core.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;

/**
 * Created by Filipe Apolinário on 01-May-15.
 */
public class GrantWorkspaceAccessDataSource {

    // Database fields
    /**
     * The database.
     */
    private SQLiteDatabase database;

    /**
     * The db helper.
     */
    private MySQLiteHelper dbHelper;

    /**
     * The all columns.
     */
    private String[] allColumns = {MySQLiteHelper.COLUMN_WS_NAME,
            MySQLiteHelper.COLUMN_REQUEST_WS_OWNER_EMAIL, MySQLiteHelper.COLUMN_REQUEST_FOREIGN_EMAIL};

    /**
     * Instantiates a new people with workspace access data source.
     *
     * @param context the application context
     */
    public GrantWorkspaceAccessDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    /**
     * Open. This method opens a connection to the AirDesk database (required for manipulating the database).
     *
     * @throws android.database.SQLException the SQL exception
     */
    public synchronized void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Close. This method closes a connection to the AirDesk database.
     */
    public synchronized void close() {
        dbHelper.close();
    }

    /**
     * Adds the workspace access to the table.
     *
     * @param workspaceAccess the workspace access
     */
    public synchronized void addWorkspaceRequestAccess(WorkspaceAccess workspaceAccess) {
        ContentValues values = new ContentValues();
        //Create People_ws_access tuple
        values.put(MySQLiteHelper.COLUMN_WS_NAME, workspaceAccess.getWorkspaceName());
        values.put(MySQLiteHelper.COLUMN_WS_OWNER_EMAIL, workspaceAccess.getWorkspaceOwnerEmail());
        values.put(MySQLiteHelper.COLUMN_FOREIGN_EMAIL,
                workspaceAccess.getEmailWithWorkspaceAccess());
        database.insert(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, null, values);
    }

    /**
     * Removes from table every Foreign User workspace access to the Workspace given in the parameter.
     *
     * @param workspaceAccess the workspace access
     */
    public synchronized void removeWorkspaceRequestAccess(WorkspaceAccess workspaceAccess) {
        database.delete(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, MySQLiteHelper.COLUMN_WS_NAME
                + " = '" + workspaceAccess.getWorkspaceName() + "'" + " AND " + MySQLiteHelper.COLUMN_REQUEST_WS_OWNER_EMAIL + " = '"
                + workspaceAccess.getWorkspaceOwnerEmail() + "'" + " AND " + MySQLiteHelper.COLUMN_REQUEST_FOREIGN_EMAIL + " = '"
                + workspaceAccess.getEmailWithWorkspaceAccess() + "'", null);
    }

    /**
     * Removes the user workspace access.
     *
     * @param name         the name
     * @param ownerEmail   the owner email
     * @param foreignEmail the foreign email
     */
    public synchronized void removeUserWorkspaceAccess(String name,
                                                       String ownerEmail,
                                                       String foreignEmail) {
        database.delete(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, MySQLiteHelper.COLUMN_WS_NAME
                + " = '" + name + "'" + " AND " + MySQLiteHelper.COLUMN_WS_OWNER_EMAIL + " = '"
                + ownerEmail + "'" + " AND " + MySQLiteHelper.COLUMN_FOREIGN_EMAIL + " = '"
                + foreignEmail + "'", null);
    }

    /**
     * Gets the workspaces access list.
     *
     * @return the workspaces access list
     */
    public synchronized List<WorkspaceAccess> getWorkspacesAccessRequestList() {
        List<WorkspaceAccess> workspacesAccessList = new ArrayList<>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, allColumns, null,
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WorkspaceAccess wsAccess = cursorToWorkspaceAccess(cursor);
            workspacesAccessList.add(wsAccess);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return workspacesAccessList;
    }

    /**
     * Cursor to workspace access.
     * Converts a database tuple (Cursor) to Workspace Access Object.
     *
     * @param cursor the cursor
     * @return the workspace access
     */
    private synchronized WorkspaceAccess cursorToWorkspaceAccess(Cursor cursor) {
        String name = cursor.getString(0);
        String ownerEmail = cursor.getString(1);
        String foreignEmail = cursor.getString(2);

        return new WorkspaceAccess(name, ownerEmail, foreignEmail);
    }

    /**
     * Find workspace access by Workspace.
     *
     * @param wsName     the ws name
     * @param ownerEmail the owner email
     * @return the array list
     */
    public synchronized ArrayList<WorkspaceAccess> findWorkspaceAccessRequestByNameAndOwner(String wsName,
                                                                                            String ownerEmail) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, allColumns,
                MySQLiteHelper.COLUMN_WS_NAME + "=? AND " + MySQLiteHelper.COLUMN_WS_OWNER_EMAIL + "=?",
                new String[]{wsName, ownerEmail}, null, null, null, null);

        ArrayList<WorkspaceAccess> ws = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ws.add(cursorToWorkspaceAccess(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();

        return ws;
    }

    /**
     * Checks if the foreign user is in workspace.
     *
     * @param wsName       the ws name
     * @param foreignEmail the foreign email
     * @return the boolean
     */
    public synchronized boolean isForeignInWorkspaceRequest(String wsName, String ownerEmail,
                                                            String foreignEmail) {

        Cursor cursor = database.query(MySQLiteHelper.TABLE_PEOPLE_WS_ACCESS_REQUEST, allColumns,
                MySQLiteHelper.COLUMN_WS_NAME + "=? AND " + MySQLiteHelper.COLUMN_WS_OWNER_EMAIL + "=? AND " + MySQLiteHelper.COLUMN_FOREIGN_EMAIL + "=?",
                new String[]{wsName, ownerEmail, foreignEmail}, null, null, null, null);

        boolean ret = false;
        if (cursor != null && cursor.getCount() > 0) {
            ret = true;
        }
        return ret;
    }


}
