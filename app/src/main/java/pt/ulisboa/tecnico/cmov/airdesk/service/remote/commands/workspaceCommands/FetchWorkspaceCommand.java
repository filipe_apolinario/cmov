package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 12-May-15.
 */
public class FetchWorkspaceCommand extends Command {
    private String worskpaceName;

    private Workspace result;

    public FetchWorkspaceCommand(String workspaceName) {
        this.worskpaceName = workspaceName;
    }

    public void execute() {
        FetchWorkspaceService service = new FetchWorkspaceService(SimpleApp.getContext());

        service.run(SimpleApp.getUserLoggedIn().getEmail(), worskpaceName);

        result = service.getResult();
    }

    public Workspace getResult() {
        return result;
    }
}
