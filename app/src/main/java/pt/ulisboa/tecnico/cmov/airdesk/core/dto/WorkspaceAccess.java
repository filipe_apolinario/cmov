package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

/**
 * Created by Group 7 on 26-Mar-15.
 */

/**
 * The Class WorkspaceAccess.
 * The main porpoise of this class is to function as a Data Transfer Object between layers.
 */
public class WorkspaceAccess extends DTO {

    /**
     * The workspace name.
     */
    private String workspaceName;

    /**
     * The workspace owner email.
     */
    private String workspaceOwnerEmail;

    /**
     * The email with workspace access.
     */
    private String emailWithWorkspaceAccess;

    public WorkspaceAccess() {

    }

    /**
     * Instantiates a new workspace access.
     *
     * @param workspaceName            the workspace name
     * @param workspaceOwnerEmail      the workspace owner email
     * @param emailWithWorkspaceAccess the email with workspace access
     */

    public WorkspaceAccess(String workspaceName,
                           String workspaceOwnerEmail,
                           String emailWithWorkspaceAccess) {
        this.workspaceName = workspaceName;
        this.workspaceOwnerEmail = workspaceOwnerEmail;
        this.emailWithWorkspaceAccess = emailWithWorkspaceAccess;
    }

    /**
     * Gets the workspace name.
     *
     * @return the workspace name
     */
    public String getWorkspaceName() {
        return workspaceName;
    }

    /**
     * Sets the workspace name.
     *
     * @param workspaceName the new workspace name
     */
    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    /**
     * Gets the workspace owner email.
     *
     * @return the workspace owner email
     */
    public String getWorkspaceOwnerEmail() {
        return workspaceOwnerEmail;
    }

    /**
     * Sets the workspace owner email.
     *
     * @param workspaceOwnerEmail the new workspace owner email
     */
    public void setWorkspaceOwnerEmail(String workspaceOwnerEmail) {
        this.workspaceOwnerEmail = workspaceOwnerEmail;
    }

    /**
     * Gets the email with workspace access.
     *
     * @return the email with workspace access
     */
    public String getEmailWithWorkspaceAccess() {
        return emailWithWorkspaceAccess;
    }

    /**
     * Sets the email with workspace access.
     *
     * @param emailWithWorkspaceAccess the new email with workspace access
     */
    public void setEmailWithWorkspaceAccess(String emailWithWorkspaceAccess) {
        this.emailWithWorkspaceAccess = emailWithWorkspaceAccess;
    }
}
