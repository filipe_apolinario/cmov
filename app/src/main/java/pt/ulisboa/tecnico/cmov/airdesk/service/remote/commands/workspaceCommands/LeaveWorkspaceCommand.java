package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserNotInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.LeaveWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class LeaveWorkspaceCommand extends Command {
    private String workspaceName;
    private String foreignEmail;
    private String result;

    public LeaveWorkspaceCommand(String workspaceName, String foreignEmail) {
        this.workspaceName = workspaceName;
        this.foreignEmail = foreignEmail;
    }

    public void execute() {
        try {
            LeaveWorkspaceService service = new LeaveWorkspaceService(SimpleApp.getContext());

            service.run(workspaceName, SimpleApp.getUserLoggedIn().getEmail(), foreignEmail);

            result = "Successfully removed User from Workspace.";
        } catch (EmptyFieldsException e) {
            this.result = "Some fields were empty.";
        } catch (UserNotInWorkspaceException e) {
            this.result = "User hasn't access to Workspace.";
        }
    }

    public String getResult() {
        return result;
    }
}
