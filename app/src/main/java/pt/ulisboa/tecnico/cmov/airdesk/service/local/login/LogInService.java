package pt.ulisboa.tecnico.cmov.airdesk.service.local.login;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.UserDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.User;

/**
 * The Class LogInService.
 * This service verifies is the User is already registered in the application and logs the user in the application.
 */
public class LogInService {

    /**
     * The database user handler.
     */
    private UserDataSource databaseUserHandler;

    /**
     * The user.
     */
    private User user;

    /**
     * Instantiates a new log in service.
     *
     * @param context the context
     */
    public LogInService(Context context) {
        databaseUserHandler = new UserDataSource(context);
    }

    /**
     * Run.
     *
     * @param email the email
     */
    public void run(String email) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {email};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        databaseUserHandler.open();

        user = databaseUserHandler.findUserByEmail(email);
        if (user == null) {
            user = new User(email);
            databaseUserHandler.addUser(user);
        }

        databaseUserHandler.close();
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

}
