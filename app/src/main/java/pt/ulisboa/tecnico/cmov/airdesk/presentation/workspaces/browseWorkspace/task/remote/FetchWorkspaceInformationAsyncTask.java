package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseForeignWorkspaceActivity;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.FetchWorkspaceCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class FetchWorkspaceAsyncTask.
 * Fetches Workspaces.
 */
public class FetchWorkspaceInformationAsyncTask extends AsyncTask<P2PNode, String, Workspace> {
    BrowseForeignWorkspaceActivity activity;
    String wsName;
    String ownerEmail;
    Context context;

    public FetchWorkspaceInformationAsyncTask(BrowseForeignWorkspaceActivity activity,
                                              Context context,
                                              String wsName,
                                              String ownerEmail) {
        this.context = context;
        this.wsName = wsName;
        this.activity = activity;
        this.ownerEmail = ownerEmail;
    }

    @Override
    protected Workspace doInBackground(P2PNode... params) {
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    FetchWorkspaceCommand command = new FetchWorkspaceCommand(wsName);

                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();
                    FetchWorkspaceCommand responseCommand = (FetchWorkspaceCommand) service.getResult();
                    publishProgress("succesfully retrived workspace");
                    return responseCommand.getResult();
                } catch (CommunicationException e) {
                    publishProgress("Failed to retrieve Workspace");
                    return null;
                }


            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    @Override
    protected void onPostExecute(Workspace ws) {
        if (ws == null) {
            return;
        }

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_about_workspace);
        dialog.setTitle(ws.getName() + " details:");

        TextView nameTextView = (TextView) dialog.findViewById(R.id.wsNameText);
        nameTextView.setText(ws.getName());

        TextView ownerEmailTextView = (TextView) dialog.findViewById(R.id.wsOwnerEmailText);
        ownerEmailTextView.setText(ws.getOwnerEmail());

        TextView quotaTextView = (TextView) dialog.findViewById(R.id.wsQuotaText);
        quotaTextView.setText(Integer.toString(ws.getQuota()) + " MB");

        TextView visibilityTextView = (TextView) dialog.findViewById(R.id.wsVisibilityText);
        visibilityTextView.setText(ws.getVisibility());

        TextView directoryTextView = (TextView) dialog.findViewById(R.id.wsDirectoryText);
        directoryTextView.setText(ws.getDirectory());

        dialog.show();
    }
}
