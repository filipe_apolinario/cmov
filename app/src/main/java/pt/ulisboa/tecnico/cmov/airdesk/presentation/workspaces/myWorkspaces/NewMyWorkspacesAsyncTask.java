package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.QuotaExceededException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceAlreadyExistsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.NewWorkspaceService;

/**
 * The Class NewMyWorkspacesAsyncTask.
 * This task Creates a Workspace.
 */
public class NewMyWorkspacesAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private NewWorkspaceService service;

    /**
     * The dialog.
     */
    private Dialog dialog;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new new my workspaces async task.
     *
     * @param c        the c
     * @param dialog   the dialog
     * @param activity the activity
     */
    public NewMyWorkspacesAsyncTask(Context c, Dialog dialog, Activity activity) {
        super();
        this.service = new NewWorkspaceService(c);
        this.dialog = dialog;
        this.activity = activity;
        this.context = c;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        String name = params[0];
        String ownerEmail = params[1];
        int quota = Integer.parseInt(params[2]);

        try {
            service.run(name, ownerEmail, quota);
            this.message = "Successfully created workspace.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (QuotaExceededException e) {
            this.message = "Quota is wrong.";
        } catch (WorkspaceAlreadyExistsException e) {
            this.message = "Workspace already exists.";
        }
        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();

        dialog.dismiss();
        reloadActivity(activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }

}
