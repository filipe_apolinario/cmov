package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 07-Apr-15.
 */


/**
 * The Class ServiceException.
 * All exceptions thrown on services are a sub-hierarchy ServiceException.
 */
public class ServiceException extends
        RuntimeException {

}
