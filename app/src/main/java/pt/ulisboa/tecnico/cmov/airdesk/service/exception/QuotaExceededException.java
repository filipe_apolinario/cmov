package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 07-Apr-15.
 */


/**
 * The Class QuotaExceededException.
 * This Exception is raised when a service is doing file operations that cause the Workspace to exceed quota.
 */
public class QuotaExceededException extends
        ServiceException {
}
