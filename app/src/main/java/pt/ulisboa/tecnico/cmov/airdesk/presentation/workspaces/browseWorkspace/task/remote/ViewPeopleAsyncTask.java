package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.ViewPeopleCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class ViewPeopleAsyncTask.
 */
public class ViewPeopleAsyncTask extends
        AsyncTask<P2PNode, String, ArrayList<WorkspaceAccess>> {

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * The workspace name.
     */
    private String workspaceName;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new view people async task.
     *
     * @param ownerEmail    the owner email
     * @param workspaceName the workspace name
     * @param activity      the activity
     */
    public ViewPeopleAsyncTask(Context context, Activity activity, String workspaceName, String ownerEmail) {
        super();
        this.context = context;
        this.activity = activity;
        this.ownerEmail = ownerEmail;
        this.workspaceName = workspaceName;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(P2PNode... params) {
        //ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    ViewPeopleCommand command = new ViewPeopleCommand(workspaceName);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();

                    ViewPeopleCommand response = (ViewPeopleCommand) service.getResult();
                    publishProgress("Succesfully received people in workspace");
                    return response.getResult();
                } catch (CommunicationException e) {
                    publishProgress("Could not perform request");
                }
            }
        }
        return null;
    }

    /**
     * On post execute.
     *
     * @param wsList the ws list
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> wsList) {
        if (wsList == null) {
            activity.finish();
            return;
        }
        ArrayList<String> list = new ArrayList<>();
        for (WorkspaceAccess ws : wsList) {
            list.add(ws.getEmailWithWorkspaceAccess());
        }

        // Create an instance of the dialog fragment and show it
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_view_people_dialog);
        TextView tView = (TextView) dialog.findViewById(R.id.ownerWorkspaceText);
        tView.setText(ownerEmail);
        dialog.setTitle("People with Workspace Access:");

        final ListView listView = (ListView) dialog.findViewById(R.id.dialogViewPeopleListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        dialog.show();
    }


}
