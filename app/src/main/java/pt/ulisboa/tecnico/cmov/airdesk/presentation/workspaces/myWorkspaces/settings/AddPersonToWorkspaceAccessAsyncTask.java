package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.AddPersonToWorkspaceAccessService;

/**
 * The Class AddPersonToWorkspaceAccessAsyncTask.
 * Adds User to Workspace
 */
public class AddPersonToWorkspaceAccessAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    final private AddPersonToWorkspaceAccessService service;

    /**
     * The context.
     */
    final private Context context;

    /**
     * The message.
     */
    private String message;

    /**
     * Instantiates a new adds the person to workspace access async task.
     *
     * @param context the context
     */
    public AddPersonToWorkspaceAccessAsyncTask(Context context) {
        super();
        this.service = new AddPersonToWorkspaceAccessService(context);
        this.context = context;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        String wsName = params[0];
        String wsOwnerEmail = params[1];
        String foreignEmail = params[2];

        try {
            service.run(wsName, wsOwnerEmail, foreignEmail);
            this.message = "User added to Workspace.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (UserAlreadyInWorkspaceException e) {
            this.message = "User is already in the Workspace.";
        } catch (WorkspaceIsPrivateException e) {
            this.message = "Cannot add User: Workspace is private.";
        }

        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
