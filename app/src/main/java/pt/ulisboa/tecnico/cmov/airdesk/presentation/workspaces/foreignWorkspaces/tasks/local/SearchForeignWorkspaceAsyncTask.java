package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.ForeignWorkspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.AskToJoinWorkspaceDialog;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspaceAdapter;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchUserWorkspacesService;

/**
 * The Class SearchForeignWorkspaceAsyncTask.
 */
public class SearchForeignWorkspaceAsyncTask extends
        AsyncTask<String, Void, ArrayList<Workspace>> {

    /**
     * The service.
     */
    private FetchUserWorkspacesService service;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    private ArrayList<ForeignWorkspace> workspacesList = new ArrayList<>();

    /**
     * Instantiates a new search foreign workspace async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public SearchForeignWorkspaceAsyncTask(Context context, ActionBarActivity activity) {
        super();
        this.service = new FetchUserWorkspacesService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<Workspace> doInBackground(String... params) {
        String ownerEmail = params[0];

        service.run(ownerEmail);

        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param workspaces the workspace accesses
     */
    @Override
    protected void onPostExecute(ArrayList<Workspace> workspaces) {
        super.onPostExecute(workspaces);

        for (Workspace w : workspaces) {
            workspacesList.add(new ForeignWorkspace(w.getName(), w.getOwnerEmail()));
        }
        if (workspacesList.isEmpty()) {
            return;
        }
        //Initialize my workspace's list
        ForeignWorkspaceAdapter adapter = new ForeignWorkspaceAdapter(activity, workspacesList, activity.getResources());
        final ListView wsListView = (ListView) activity.findViewById(R.id.searchForeignWsList);
        final AdapterView.OnItemClickListener wsListViewItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = wsListView.getPositionForView(myView);
                ForeignWorkspace title = (ForeignWorkspace) workspacesList.get(position);
                Bundle args = new Bundle();
                args.putString("title", title.getWorkspaceName());
                args.putString("owner_email", title.getWorkspaceOwner());
                AskToJoinDialog(args);
            }
        };
        wsListView.setOnItemClickListener(wsListViewItemClickListener);
        wsListView.setAdapter(adapter);
    }

    /**
     * Show workspace notice dialog.
     *
     * @param args the args
     */
    private void AskToJoinDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        AskToJoinWorkspaceDialog dialog = new AskToJoinWorkspaceDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
