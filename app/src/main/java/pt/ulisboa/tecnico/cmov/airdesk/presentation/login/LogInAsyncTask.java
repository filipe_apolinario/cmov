package pt.ulisboa.tecnico.cmov.airdesk.presentation.login;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks.RequestEmailTask;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.User;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.MyWorkspacesActivity;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.login.LogInService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.ExchangeEmailCommand;


/**
 * @author Group 7
 * @since 22-Mar-15.
 * Calls the service to perform the login of the given user and then calls the user's MyWorkspacesActivity.
 */
public class LogInAsyncTask extends
        AsyncTask<String, Void, User> {

    Context context;

    /**
     * The service.
     */
    private LogInService service;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * Instantiates a new log in async task.
     *
     * @param activity the activity
     * @param context  the context
     */
    public LogInAsyncTask(ActionBarActivity activity, Context context) {
        super();
        this.service = new LogInService(context);
        this.activity = activity;
        this.context = context;
    }

    /**
     * Do in background.
     *
     * @param params first argument must be the user email.
     * @return registered user
     */
    @Override
    protected User doInBackground(String... params) {
        //Background task responsible for fetching the user from the database and return to the main task.
        //If the user does not exist, the background task creates a new representation of the user on the database.
        String email = params[0];

        service.run(email);
        return service.getUser();
    }

    /**
     * On post execute.
     *
     * @param user the user
     */
    @Override
    protected void onPostExecute(User user) {
        //After the background task ran.
        //Main thread sets the application context to the logged in user
        if (user == null) {
            return;
        }
        SimpleApp.setUserLoggedIn(user);
        goToUserWorkspaces();

        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
        ExchangeEmailCommand command = new ExchangeEmailCommand(user.getEmail(), SimpleApp.getWifiDirectStateKeeper().getDeviceName());

        for (P2PNode peer : peerList) {
            new RequestEmailTask(context, command).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, peer);
        }
    }

    /**
     * Go to user workspaces.
     */
    private void goToUserWorkspaces() {
        Intent intent = new Intent(activity, MyWorkspacesActivity.class);
        activity.startActivity(intent);
    }


}
