package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserAlreadyInWorkspaceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceIsPrivateException;

/**
 * The Class AddPersonToWorkspaceAccessService.
 */
public class AddPersonToWorkspaceAccessService {

    /**
     * The database people with workspace access handler.
     */
    private WorkspaceAccessDataSource databasePeopleWithWorkspaceAccessHandler;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new adds the person to workspace access service.
     *
     * @param context the context
     */
    public AddPersonToWorkspaceAccessService(Context context) {
        this.context = context;
        databasePeopleWithWorkspaceAccessHandler = new WorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param wsName       the ws name
     * @param wsOwnerEmail the ws owner email
     * @param foreignEmail the foreign email
     */
    public void run(String wsName, String wsOwnerEmail, String foreignEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] params = {wsName, wsOwnerEmail, foreignEmail};
        if (!verifier.stringVerifier(params)) {
            throw new EmptyFieldsException();
        }

        FetchWorkspaceService service = new FetchWorkspaceService(context);
        service.run(wsOwnerEmail, wsName);
        Workspace ws = service.getResult();
        //Verify workspace exists and it is a public Workspace.
        if (ws == null) {
            return;
        }
        if (ws.isPrivateWorkspace()) {
            throw new WorkspaceIsPrivateException();
        }
        //Ok lets add a person to the database!
        databasePeopleWithWorkspaceAccessHandler.open();
        if (databasePeopleWithWorkspaceAccessHandler.isForeignInWorkspace(wsName, wsOwnerEmail, foreignEmail)) {
            throw new UserAlreadyInWorkspaceException();
        }
        WorkspaceAccess wsA = new WorkspaceAccess(wsName, wsOwnerEmail, foreignEmail);
        databasePeopleWithWorkspaceAccessHandler.addWorkspaceAccess(wsA);

        databasePeopleWithWorkspaceAccessHandler.close();
    }
}
