package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces;

/**
 * Created by Group 7 on 15-Mar-15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ListView;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseMyWorkspaceActivity;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings.SettingsActivity;

/**
 * The Class WorkspaceDialog.
 */
public class WorkspaceDialog extends
        DialogFragment {

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String WORKSPACE_NAME = getArguments().getString("title");
        final String OWNER_EMAIL = getArguments().getString("owner_email");
        builder.setTitle(WORKSPACE_NAME);
        builder.setItems(R.array.my_workspace_dialog_array, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // The 'which' argument contains the index position
                // of the selected item

                String valueOptionPressed = getMyWorkspaceOptionPressed((Dialog) dialog, which);

                final String BROWSE_WORKSPACE = getResources().getText(
                        R.string.dialog_browse_workspace).toString();
                final String SETTINGS_WORKSPACE = getResources().getText(
                        R.string.dialog_workspace_settings).toString();
                final String DESTROY_WORKSPACE = getResources().getText(
                        R.string.dialog_destroy_workspace).toString();

                if (valueOptionPressed.equals(BROWSE_WORKSPACE)) {
                    Intent intent = new Intent(getActivity(), BrowseMyWorkspaceActivity.class);
                    intent.putExtra("workspace", WORKSPACE_NAME);
                    intent.putExtra("owner_email", OWNER_EMAIL);
                    startActivity(intent);
                } else if (valueOptionPressed.equals(SETTINGS_WORKSPACE)) {
                    Intent intent = new Intent(getActivity(), SettingsActivity.class);
                    intent.putExtra("workspace", WORKSPACE_NAME);
                    startActivity(intent);
                } else if (valueOptionPressed.equals(DESTROY_WORKSPACE)) {
                    DestroyWorkspaceAsyncTask task = new DestroyWorkspaceAsyncTask(getActivity(),
                            getActivity().getApplicationContext(), WORKSPACE_NAME, SimpleApp
                            .getUserLoggedIn().getEmail());
                    task.execute();

                    //This seems to be a dangerous option, there should be some confirmation before executing any operation.
                    //I suggest another dialog to test if the user really wants to leave the workspace.
                }
            }

            private String getMyWorkspaceOptionPressed(Dialog dialog, int which) {
                ListView v = (ListView) dialog.getCurrentFocus();
                return v.getItemAtPosition(which).toString();
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

}
