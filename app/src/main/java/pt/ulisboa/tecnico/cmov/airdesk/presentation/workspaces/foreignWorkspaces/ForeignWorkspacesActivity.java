package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote.FindForeignWorkspaceAsyncTask;
//import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local.FindForeignWorkspaceAsyncTask;


/**
 * The Class ForeignWorkspacesActivity.
 * Activity that lists all the foreign workspaces and lets you perform operations in workspace.
 */
public class ForeignWorkspacesActivity extends
        ActionBarActivity {

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foreign_workspaces);
        if (SimpleApp.getWifiDirectStateKeeper().isWifiEnabled()) {
            //remote implementation
            ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
            P2PNode[] peerArray = new P2PNode[peerList.size()];
            peerArray = peerList.toArray(peerArray);
            FindForeignWorkspaceAsyncTask task = new FindForeignWorkspaceAsyncTask(getApplicationContext(), this, SimpleApp.getUserLoggedIn().getEmail());

            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, peerArray);
        } else {
            //local implementation
            pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local.FindForeignWorkspaceAsyncTask task = new pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.local.FindForeignWorkspaceAsyncTask(
                    getApplicationContext(), this);
            task.execute(SimpleApp.getUserLoggedIn().getEmail());
        }
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_foreign_workspaces, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Search foreign workspaces.
     *
     * @param v the v
     */
    public void searchForeignWorkspaces(View v) {
        Intent intent = new Intent(this, SearchWorkspaceActivity.class);
        startActivity(intent);
    }

    public void viewMyWorkspaces(View v) {
        finish();
    }

    /**
     * Show foreign workspace notice dialog.
     *
     * @param args the args
     */
    private void showForeignWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        if (SimpleApp.getWifiDirectStateKeeper().isWifiEnabled()) {
            ForeignWorkspaceOnlineDialog dialog = new ForeignWorkspaceOnlineDialog();
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
        } else {
            ForeignWorkspaceOfflineDialog dialog = new ForeignWorkspaceOfflineDialog();
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
        }
    }
}
