package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class FindMyForeignWorkspaceService.
 */
public class FindMyPublicWorkspaceService {

    /**
     * The data source.
     */
    private WorkspaceDataSource dataSource;

    /**
     * The workspace accesses list.
     */
    private ArrayList<Workspace> workspaceList;

    /**
     * Instantiates a new find my foreign workspace service.
     *
     * @param context the context
     */
    public FindMyPublicWorkspaceService(Context context) {
        this.dataSource = new WorkspaceDataSource(context);
    }

    /**
     * Run.
     *
     * @param ownerEmail the email
     */
    public void run(String ownerEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        dataSource.open();

        workspaceList = dataSource.findPublicWorkspaceByOwner(ownerEmail);

        dataSource.close();

    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public ArrayList<Workspace> getResult() {
        return workspaceList;
    }

}
