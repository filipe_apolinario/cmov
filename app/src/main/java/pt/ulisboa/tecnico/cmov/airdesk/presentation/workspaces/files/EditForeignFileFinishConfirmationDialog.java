package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files;

/**
 * Created by Group 7 on 14-Mar-15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.remote.EditFileAsyncTask;

/**
 * The Class EditFileFinishConfirmationDialog.
 */
public class EditForeignFileFinishConfirmationDialog extends
        DialogFragment {

    /**
     * The file name.
     */
    private String FILE_NAME;

    /**
     * The owner email.
     */
    private String OWNER_EMAIL;
    /**
     * The Workspace name.
     */
    private String WORKSPACE_NAME;

    /**
     * The text.
     */
    private String TEXT;

    /**
     * The quota.
     */
    private int QUOTA;

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        Bundle args = this.getArguments();

        FILE_NAME = args.getString("file_name");
        WORKSPACE_NAME = args.getString("workspace_name");
        OWNER_EMAIL = args.getString("owner_email");
        TEXT = args.getString("body");
        QUOTA = args.getInt("quota");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Save and Exit");

        builder.setMessage("Do you want save and exit the file editor?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Activity activity = getActivity();
                        ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
                        P2PNode[] peerArray = new P2PNode[peerList.size()];
                        peerArray = peerList.toArray(peerArray);
                        EditFileAsyncTask task = new EditFileAsyncTask(activity
                                .getApplicationContext(), getActivity(), OWNER_EMAIL, WORKSPACE_NAME,
                                FILE_NAME, TEXT, QUOTA);
                        task.execute(peerArray);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Intentionally left black, user cancelled the dialog
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
