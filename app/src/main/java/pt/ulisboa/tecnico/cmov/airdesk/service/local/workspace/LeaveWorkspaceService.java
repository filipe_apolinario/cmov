package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.UserNotInWorkspaceException;

/**
 * The Class LeaveWorkspaceService.
 */
public class LeaveWorkspaceService {

    /**
     * The data source access handler.
     */
    private WorkspaceAccessDataSource dataSourceAccessHandler;

    /**
     * Instantiates a new leave workspace service.
     *
     * @param context the context
     */
    public LeaveWorkspaceService(Context context) {
        dataSourceAccessHandler = new WorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param workspaceName the workspace name
     * @param ownerEmail    the owner email
     * @param foreignEmail  the foreign email
     */
    public void run(String workspaceName, String ownerEmail, String foreignEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] params = {workspaceName, ownerEmail, foreignEmail};
        if (!verifier.stringVerifier(params)) {
            throw new EmptyFieldsException();
        }

        removeFromDatabase(workspaceName, ownerEmail, foreignEmail);
    }

    /**
     * Removes the from database.
     *
     * @param wsName       the ws name
     * @param ownerEmail   the owner email
     * @param foreignEmail the foreign email
     */
    private void removeFromDatabase(String wsName, String ownerEmail, String foreignEmail) {

        dataSourceAccessHandler.open();
        if (!dataSourceAccessHandler.isForeignInWorkspace(wsName, ownerEmail, foreignEmail)) {
            throw new UserNotInWorkspaceException();
        }
        dataSourceAccessHandler.removeUserWorkspaceAccess(wsName, ownerEmail, foreignEmail);

        dataSourceAccessHandler.close();

    }
}
