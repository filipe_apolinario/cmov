package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.remote;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.CreateFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands.CreateFileCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class CreateFileAsyncTask.
 * creates the file in FileStorage.
 */
public class CreateFileAsyncTask extends
        AsyncTask<P2PNode, String, Void> {

    /**
     * The service.
     */
    private CreateFileService service;

    /**
     * The dialog.
     */
    private Dialog dialog;

    /**
     * The ws.
     */
    private Workspace ws;

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * The Workspace name.
     */
    private String workspaceName;

    /**
     * The Owner Email.
     */
    private String ownerEmail;

    /**
     * Instantiates a new creates the file async task.
     *
     * @param c        the c
     * @param dialog   the dialog
     * @param activity the activity
     * @param fileName the file name
     */
    public CreateFileAsyncTask(Context c,
                               Dialog dialog,
                               Activity activity,
                               String ownerEmail,
                               String workspaceName,
                               String fileName) {
        super();
        this.service = new CreateFileService(c);
        this.dialog = dialog;
        this.workspaceName = workspaceName;
        this.fileName = fileName;
        this.ownerEmail = ownerEmail;
        this.activity = activity;
        this.context = c;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(P2PNode... params) {
        ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    CreateFileCommand command = new CreateFileCommand(workspaceName, fileName);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();

                    CreateFileCommand response = (CreateFileCommand) service.getResult();
                    publishProgress(response.getResult());

                } catch (CommunicationException e) {
                    publishProgress("Could not perform request");
                }
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        dialog.dismiss();
        reloadActivity(this.activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }


}
