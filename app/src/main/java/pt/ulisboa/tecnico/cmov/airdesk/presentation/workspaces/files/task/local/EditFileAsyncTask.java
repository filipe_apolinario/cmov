package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.WriteFileService;

/**
 * The Class EditFileAsyncTask.
 * This task updates the content of the file to the FileStorage
 */
public class EditFileAsyncTask extends
        AsyncTask<String, Void, String> {

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The folder absolute path.
     */
    private String folderAbsolutePath;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The service.
     */
    private WriteFileService service;

    /**
     * The text.
     */
    private String text;

    /**
     * The quota.
     */
    private int quota;

    /**
     * The context.
     */
    private Context context;


    /**
     * Instantiates a new edits the file async task.
     *
     * @param c                  the c
     * @param activity           the activity
     * @param folderAbsolutePath the folder absolute path
     * @param fileName           the file name
     * @param text               the text
     * @param quota              the quota
     */
    public EditFileAsyncTask(Context c,
                             Activity activity,
                             String folderAbsolutePath,
                             String fileName,
                             String text,
                             int quota) {
        super();

        this.service = new WriteFileService(c);
        this.fileName = fileName;
        this.folderAbsolutePath = folderAbsolutePath;
        this.activity = activity;
        this.text = text;
        this.quota = quota;
        this.context = c;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Saving file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the string
     */
    @Override
    protected String doInBackground(String... params) {
        service.run(folderAbsolutePath, fileName, text, quota);

        if (service.getResult()) {
            return "Saved File Successfully";
        } else {
            return "File too big: Please raise Workspace quota or delete some files.";
        }
    }

    /**
     * On post execute.
     *
     * @param result the result
     */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, result, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
        activity.finish();
    }
}
