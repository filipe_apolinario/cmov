package pt.ulisboa.tecnico.cmov.airdesk.service.local.file;

/**
 * Created by Group 7 on 06-Apr-15.
 * this task search files in Workspace's directory.
 */

import android.content.Context;

import java.io.File;
import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;

/**
 * The Class SearchFilesInWorkspaceService.
 */
public class SearchFilesInWorkspaceService {

    /**
     * The workspace path.
     */
    private String workspacePath;

    /**
     * The context.
     */
    private Context context;

    /**
     * The file list.
     */
    private ArrayList<String> fileList;

    {
        this.fileList = new ArrayList<>();
    }

    /**
     * Instantiates a new search files in workspace service.
     *
     * @param context       the context
     * @param workspacePath the workspace path
     */
    public SearchFilesInWorkspaceService(Context context, String workspacePath) {
        this.context = context;
        this.workspacePath = workspacePath;
    }

    /**
     * Run.
     */
    public void run() {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {workspacePath};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        FileStorageHandler handler = new FileStorageHandler();
        String applicationPath = context.getFilesDir().getAbsolutePath();
        String wsAbsPath = applicationPath + File.separator + workspacePath;

        if (!handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }

        this.fileList = handler.getNameOfFilesInWorkspace(context.getFilesDir() + File.separator
                + workspacePath);
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public ArrayList<String> getResult() {
        return fileList;
    }

}
