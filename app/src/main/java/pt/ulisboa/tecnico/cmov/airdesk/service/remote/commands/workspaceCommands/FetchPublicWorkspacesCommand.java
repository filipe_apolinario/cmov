package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FindMyPublicWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class FetchPublicWorkspacesCommand extends Command {
    private ArrayList<Workspace> result;

    public FetchPublicWorkspacesCommand() {

    }

    public void execute() {
        String ownerEmail = SimpleApp.getUserLoggedIn().getEmail();
        FindMyPublicWorkspaceService service = new FindMyPublicWorkspaceService(SimpleApp.getContext());

        service.run(ownerEmail);

        result = service.getResult();
    }

    public ArrayList<Workspace> getResult() {
        return result;
    }
}
