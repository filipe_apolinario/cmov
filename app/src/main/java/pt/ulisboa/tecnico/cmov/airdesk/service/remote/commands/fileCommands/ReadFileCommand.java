package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.ReadFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class ReadFileCommand extends Command {
    private String workspaceName;
    private String fileName;

    private String result;

    public ReadFileCommand(String workspaceName, String fileName) {
        this.workspaceName = workspaceName;
        this.fileName = fileName;
    }

    public void execute() {
        try {
            FetchWorkspaceService workspaceService = new FetchWorkspaceService(SimpleApp.getContext());
            workspaceService.run(SimpleApp.getUserLoggedIn().getEmail(), workspaceName);

            Workspace ws = workspaceService.getResult();

            ReadFileService service = new ReadFileService(SimpleApp.getContext());
            service.run(ws.getDirectory(), fileName);
            result = service.getResult();
        } catch (EmptyFieldsException e) {
            result = "Some fields were empty.";
        } catch (ServiceException e) {
            result = "An error ocurred.";
        }
    }

    public String getResult() {
        return result;
    }
}
