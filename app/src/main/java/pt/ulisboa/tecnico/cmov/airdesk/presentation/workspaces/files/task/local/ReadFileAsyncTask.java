package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.files.task.local;

/**
 * Created by Group 7 on 07-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.ReadFileService;

/**
 * The Class ReadFileAsyncTask.
 * Fetches the content of the file.
 */
public class ReadFileAsyncTask extends
        AsyncTask<String, Void, String> {

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The folder absolute path.
     */
    private String folderAbsolutePath;

    /**
     * The service.
     */
    private ReadFileService service;

    /**
     * The read file content text.
     */
    private TextView readFileContentText;

    /**
     * The context.
     */
    private Context context;


    /**
     * Instantiates a new read file async task.
     *
     * @param c                  the c
     * @param folderAbsolutePath the folder absolute path
     * @param fileName           the file name
     * @param textView           the text view
     */
    public ReadFileAsyncTask(Context c,
                             String folderAbsolutePath,
                             String fileName,
                             TextView textView) {
        super();

        this.service = new ReadFileService(c);
        this.fileName = fileName;
        this.folderAbsolutePath = folderAbsolutePath;
        this.readFileContentText = textView;
        this.context = c;
    }

    /**
     * On pre execute.
     */
    @Override
    protected void onPreExecute() {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Reading file: Please wait.", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the string
     */
    @Override
    protected String doInBackground(String... params) {

        service.run(folderAbsolutePath, fileName);
        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param result the result
     */
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Read File Successfully", duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();

        this.readFileContentText.setText(result);
    }
}
