package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 26-Mar-15.
 */

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.QuotaBoundariesOfWorkspaceService;

/**
 * The Class SettingsActivity.
 * In this activity the user can do several operations on the workspace.
 * View information of Workspace
 * Add User to Workspace
 * Change Workspace's visibility
 * Change Workspace's quota
 */
public class SettingsActivity extends
        ActionBarActivity {

    /**
     * The workspace name.
     */
    private String WORKSPACE_NAME;

    /**
     * The ws.
     */
    private Workspace ws;

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_workspace_settings);

        Intent i = getIntent();
        WORKSPACE_NAME = i.getStringExtra("workspace");
        FetchWorkspaceService service = new FetchWorkspaceService(getApplicationContext());
        service.run(SimpleApp.getUserLoggedIn().getEmail(), WORKSPACE_NAME);
        this.ws = service.getResult();
    }

    /**
     * On create options menu.
     *
     * @param menu the menu
     * @return true, if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_workspace_settings, menu);
        return true;
    }

    /**
     * On options item selected.
     *
     * @param item the item
     * @return true, if successful
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * View user.
     *
     * @param v the v
     */
    public void viewUser(View v) {
        ViewUserWorkspaceAccessAsyncTask task = new ViewUserWorkspaceAccessAsyncTask(
                getApplicationContext(), this);
        String[] params = {WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail()};
        task.execute(params);
    }

    /**
     * View user waiting for workspace access.
     *
     * @param v the v
     */
    public void viewUserWaitingForWorkspaceAccess(View v) {
        ViewUserWaitingAccessAsyncTask task = new ViewUserWaitingAccessAsyncTask(getApplicationContext(), this);
        task.execute(WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail());
    }

    /**
     * Adds the person to workspace dialog.
     *
     * @param v the v
     */
    public void addUserToWorkspaceDialog(View v) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_person_workspace);
        dialog.setTitle("Add User to " + WORKSPACE_NAME);
        Button submitButton = (Button) dialog.findViewById(R.id.addPersonButton);
        // if button is clicked, close the custom dialog
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tView = (EditText) dialog.findViewById(R.id.addPersonEditText);
                String email = tView.getText().toString();
                AddPersonToWorkspaceAccessAsyncTask task = new AddPersonToWorkspaceAccessAsyncTask(
                        getApplicationContext());
                String[] params = {WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail(), email};
                task.execute(params);
                dialog.dismiss();
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelAddPersonButton);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        /*AddPersonWorkspaceDialog dialog = new AddPersonWorkspaceDialog();
        Bundle args = new Bundle();
        args.putString("title", WORKSPACE_NAME);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");*/
    }

    /**
     * Change workspace quota.
     *
     * @param v the v
     */
    public void changeWorkspaceQuota(View v) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_change_quota);
        dialog.setTitle("Change " + WORKSPACE_NAME + "'s quota");
        Button submitButton = (Button) dialog.findViewById(R.id.okChangeQuotaButton);

        final SeekBar quotaBar = (SeekBar) dialog.findViewById(R.id.changeQuotaBar);
        final TextView quotaValue = (TextView) dialog.findViewById(R.id.changeQuotaText);


        // Initialize the textview with '0'.
        QuotaBoundariesOfWorkspaceService service = new QuotaBoundariesOfWorkspaceService(
                getApplicationContext());
        service.run(ws);
        final long[] quotaBoundaries = service.getResult();
        final int minQuota = quotaBoundaries[0] < 1 ? 1 : (int) quotaBoundaries[0];
        final int maxQuota = (int) quotaBoundaries[1] - 1;

        quotaValue.setText(minQuota + " MB");
        quotaBar.setMax(maxQuota - minQuota);

        quotaBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                quotaValue.setText((progressValue + minQuota) + " MB");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // if button is clicked, close the custom dialog
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EditText tView = (EditText) dialog.findViewById(R.id.changeQuotaEditText);
                int quota = quotaBar.getProgress() + minQuota;

                ChangeWorkspaceQuotaAsyncTask task = new ChangeWorkspaceQuotaAsyncTask(
                        getApplicationContext());
                String[] params = {WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail(),
                        Integer.toString(quota)};
                task.execute(params);
                ws.setQuota(quota);
                dialog.dismiss();
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelChangeQuotaButton);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    /**
     * Change workspace visibility.
     *
     * @param v the v
     */
    public void changeWorkspaceVisibility(View v) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_change_visibility);
        dialog.setTitle("Change " + WORKSPACE_NAME + "'s visibility");

        Button submitButton = (Button) dialog.findViewById(R.id.okChangeVisibilityButton);
        CheckBox tView = (CheckBox) dialog.findViewById(R.id.changeVisibilityWsPublicCheckBox);
        tView.setChecked(ws.isPrivateWorkspace());
        // if button is clicked, close the custom dialog
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox tView = (CheckBox) dialog
                        .findViewById(R.id.changeVisibilityWsPublicCheckBox);

                boolean isPrivateWorkspace = tView.isChecked();
                ws.setPrivateWorkspace(isPrivateWorkspace);
                ChangeWorkspaceVisibilityAsyncTask task = new ChangeWorkspaceVisibilityAsyncTask(
                        getApplicationContext());
                String[] params = {WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail(),
                        Boolean.toString(isPrivateWorkspace)};
                task.execute(params);
                dialog.dismiss();
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.cancelChangeVisibilityButton);
        // if button is clicked, close the custom dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * About workspace.
     *
     * @param v the v
     */
    public void aboutWorkspace(View v) {

        FetchWorkspaceInformationAsyncTask task = new FetchWorkspaceInformationAsyncTask(this.getApplicationContext(),
                this);
        String[] params = {WORKSPACE_NAME, SimpleApp.getUserLoggedIn().getEmail()};
        task.execute(params);
    }
}
