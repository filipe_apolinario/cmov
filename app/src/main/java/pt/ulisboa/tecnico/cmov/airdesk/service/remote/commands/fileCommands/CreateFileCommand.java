package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.fileCommands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ErrorOccurredException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.FileAlreadyExistsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.CreateFileService;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 13-May-15.
 */
public class CreateFileCommand extends Command {
    private String workspaceName;
    private String fileName;

    private String result;

    public CreateFileCommand(String workspaceName, String fileName) {
        this.workspaceName = workspaceName;
        this.fileName = fileName;
    }

    public void execute() {
        try {
            FetchWorkspaceService workspaceService = new FetchWorkspaceService(SimpleApp.getContext());
            workspaceService.run(SimpleApp.getUserLoggedIn().getEmail(), workspaceName);

            Workspace ws = workspaceService.getResult();

            CreateFileService service = new CreateFileService(SimpleApp.getContext());
            service.run(ws, fileName);
            result = "Successfully created file.";
        } catch (EmptyFieldsException e) {
            result = "Some fields were empty.";
        } catch (FileAlreadyExistsException e) {
            result = "File already exists.";
        } catch (ErrorOccurredException e) {
            result = "An error occurred.";
        } catch (ServiceException e) {
            result = "An error ocurred.";
        }
    }

    public String getResult() {
        return result;
    }
}
