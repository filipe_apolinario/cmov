package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

import android.content.Context;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.GrantWorkspaceAccessDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.WorkspaceDoesNotExistException;

/**
 * Created by Group 7 on 31-Mar-15.
 */
public class FetchRequestsWorkspaceAccessService {

    /**
     * The database my workspace handler.
     */
    private GrantWorkspaceAccessDataSource databaseMyWorkspaceHandler;

    /**
     * The people with workspace access.
     */
    private ArrayList<WorkspaceAccess> peopleWithWorkspaceAccess;

    /**
     * The context.
     */
    private Context context;

    {
        peopleWithWorkspaceAccess = new ArrayList<>();
    }

    /**
     * Instantiates a new fetch workspace access service.
     *
     * @param context the context
     */
    public FetchRequestsWorkspaceAccessService(Context context) {
        this.context = context;
        databaseMyWorkspaceHandler = new GrantWorkspaceAccessDataSource(context);
    }

    /**
     * Run.
     *
     * @param wsName     the ws name
     * @param ownerEmail the owner email
     */
    public void run(String wsName, String ownerEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {wsName, ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }

        FetchWorkspaceService service = new FetchWorkspaceService(context);
        service.run(ownerEmail, wsName);
        Workspace ws = service.getResult();
        //Verify workspace exists and it is a public Workspace.
        if (ws == null || ws.isPrivateWorkspace()) {
            throw new WorkspaceDoesNotExistException();
        }
        //Ok lets see in the database people who have workspace access!
        databaseMyWorkspaceHandler.open();

        peopleWithWorkspaceAccess = databaseMyWorkspaceHandler
                .findWorkspaceAccessRequestByNameAndOwner(wsName, ownerEmail);

        databaseMyWorkspaceHandler.close();
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public ArrayList<WorkspaceAccess> getResult() {
        return peopleWithWorkspaceAccess;
    }
}
