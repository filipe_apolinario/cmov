package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchUserWorkspacesService;

/**
 * The Class FetchWorkspaceAsyncTask.
 * Fetches Workspaces.
 */
public class FetchWorkspaceInformationAsyncTask extends
        AsyncTask<String, Void, Workspace> {

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The service.
     */
    private FetchUserWorkspacesService service;

    /**
     * Instantiates a new fetch workspace async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public FetchWorkspaceInformationAsyncTask(Context context, ActionBarActivity activity) {
        super();
        service = new FetchUserWorkspacesService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the workspace
     */
    @Override
    protected Workspace doInBackground(String... params) {
        String wsName = params[0];
        String ownerEmail = params[1];

        service.run(ownerEmail);
        ArrayList<Workspace> wsList = service.getResult();

        for (Workspace ws : wsList) {
            if (ws.getName().equals(wsName)) {
                return ws;
            }
        }
        return null;
    }

    /**
     * On post execute.
     *
     * @param ws the ws
     */
    @Override
    protected void onPostExecute(Workspace ws) {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_about_workspace);
        dialog.setTitle(ws.getName() + " details:");

        TextView nameTextView = (TextView) dialog.findViewById(R.id.wsNameText);
        nameTextView.setText(ws.getName());

        TextView ownerEmailTextView = (TextView) dialog.findViewById(R.id.wsOwnerEmailText);
        ownerEmailTextView.setText(ws.getOwnerEmail());

        TextView quotaTextView = (TextView) dialog.findViewById(R.id.wsQuotaText);
        quotaTextView.setText(Integer.toString(ws.getQuota()) + " MB");

        TextView visibilityTextView = (TextView) dialog.findViewById(R.id.wsVisibilityText);
        visibilityTextView.setText(ws.getVisibility());

        TextView directoryTextView = (TextView) dialog.findViewById(R.id.wsDirectoryText);
        directoryTextView.setText(ws.getDirectory());

        dialog.show();
    }
}
