package pt.ulisboa.tecnico.cmov.airdesk;

/**
 * Created by Group 7 on 07-Apr-15.
 */

/**
 * The Class FieldVerifier.
 * Auxiliary class that verifies the fields in the activity.
 */
public class FieldVerifier {

    /**
     * String verifier.
     *
     * @param params the params
     * @return true, if successful
     */
    public boolean stringVerifier(String... params) {
        for (String param : params) {
            if (param == null || param.length() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Positive verifier.
     *
     * @param params the params
     * @return true, if successful
     */
    public boolean positiveVerifier(int... params) {
        for (int param : params) {
            if (param >= Integer.MAX_VALUE || param < 0) {
                return false;
            }
        }
        return true;
    }
}
