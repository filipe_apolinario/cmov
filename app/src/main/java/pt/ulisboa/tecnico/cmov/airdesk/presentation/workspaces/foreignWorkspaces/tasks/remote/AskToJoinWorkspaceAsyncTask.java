package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.AskToJoinWorkspaceCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * Created by Filipe Apolinário on 17-Apr-15.
 */
public class AskToJoinWorkspaceAsyncTask extends
        AsyncTask<P2PNode, String, Void> {

    /**
     * The context.
     */
    private Context context;

    private String workspaceName;

    private String ownerEmail;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    private String foreignEmail;

    /**
     * Instantiates a new fetch user workspaces async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public AskToJoinWorkspaceAsyncTask(Context context, Activity activity, String ownerEmail, String name, String foreignEmail) {
        this.context = context;
        this.activity = activity;
        this.ownerEmail = ownerEmail;
        this.workspaceName = name;
        this.foreignEmail = foreignEmail;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected Void doInBackground(P2PNode... params) {
        ArrayList<Workspace> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            if (peer.getOwnerEmail().equals(ownerEmail)) {
                try {
                    AskToJoinWorkspaceCommand command = new AskToJoinWorkspaceCommand(workspaceName, foreignEmail);

                    //send request to peer
                    publishProgress("Sending request to peer");
                    RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                    service.execute();

                    AskToJoinWorkspaceCommand response = (AskToJoinWorkspaceCommand) service.getResult();
                    publishProgress(response.getResult());

                } catch (CommunicationException e) {
                    publishProgress("Could not perform request");
                }
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, values[0], duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
