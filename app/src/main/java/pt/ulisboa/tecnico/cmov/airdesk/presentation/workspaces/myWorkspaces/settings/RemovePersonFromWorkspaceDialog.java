package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.myWorkspaces.settings;

/**
 * Created by Group 7 on 14-Mar-15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;

/**
 * The Class RemovePersonFromWorkspaceDialog.
 * Remove User from Workspace
 */
public class RemovePersonFromWorkspaceDialog extends
        DialogFragment {

    /**
     * On create dialog.
     *
     * @param savedInstanceState the saved instance state
     * @return the dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String WORKSPACE_NAME = getArguments().getString("workspace");
        final String PERSON_NAME = getArguments().getString("person");
        builder.setTitle("Remove Person From " + "'" + WORKSPACE_NAME + "'");

        builder.setMessage(
                "Do you want to remove " + "'" + PERSON_NAME + "'" + " from " + "'"
                        + WORKSPACE_NAME + "'")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Activity activity = getActivity();

                        RemovePersonFromWorkspaceAsyncTask task = new RemovePersonFromWorkspaceAsyncTask(
                                activity.getApplicationContext(), WORKSPACE_NAME, SimpleApp
                                .getUserLoggedIn().getEmail(), PERSON_NAME);
                        task.execute();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Intentionally left black, user cancelled the dialog
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
