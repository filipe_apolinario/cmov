package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;

/**
 * The Class FetchUserWorkspacesService.
 */
public class FetchUserWorkspacesService {

    /**
     * The ws list.
     */
    private ArrayList<Workspace> wsList;

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * Instantiates a new fetch user workspaces service.
     *
     * @param context the context
     */
    public FetchUserWorkspacesService(Context context) {
        super();
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
    }

    /**
     * Run.
     *
     * @param ownerEmail the owner email
     */
    public void run(String ownerEmail) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {ownerEmail};
        if (!verifier.stringVerifier(sParams)) {
            return;
        }
        databaseMyWorkspaceHandler.open();

        wsList = databaseMyWorkspaceHandler.findWorkspaceByOwner(ownerEmail);

        databaseMyWorkspaceHandler.close();
    }

    /**
     * Gets the result.
     *
     * @return the result
     */
    public ArrayList<Workspace> getResult() {
        return wsList;
    }
}
