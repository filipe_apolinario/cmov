package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.local;

/**
 * Created by Group 7 on 22-Mar-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceAccessService;

/**
 * The Class ViewPeopleAsyncTask.
 */
public class ViewPeopleAsyncTask extends
        AsyncTask<String, Void, ArrayList<WorkspaceAccess>> {

    /**
     * The service.
     */
    private FetchWorkspaceAccessService service;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The owner email.
     */
    private String ownerEmail;

    /**
     * Instantiates a new view people async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public ViewPeopleAsyncTask(Context context, Activity activity) {
        super();
        this.service = new FetchWorkspaceAccessService(context);
        this.activity = activity;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(String... params) {
        String wsName = params[0];
        this.ownerEmail = params[1];

        service.run(wsName, ownerEmail);

        return service.getResult();
    }

    /**
     * On post execute.
     *
     * @param wsList the ws list
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> wsList) {
        ArrayList<String> list = new ArrayList<>();
        for (WorkspaceAccess ws : wsList) {
            list.add(ws.getEmailWithWorkspaceAccess());
        }

        // Create an instance of the dialog fragment and show it
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_view_people_dialog);
        TextView tView = (TextView) dialog.findViewById(R.id.ownerWorkspaceText);
        tView.setText(ownerEmail);
        dialog.setTitle("People with Workspace Access:");

        final ListView listView = (ListView) dialog.findViewById(R.id.dialogViewPeopleListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        dialog.show();
    }


}
