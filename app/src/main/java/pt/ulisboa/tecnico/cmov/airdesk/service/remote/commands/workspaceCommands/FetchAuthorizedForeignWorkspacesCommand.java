package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FindMyForeignWorkspaceService;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.Command;

/**
 * Created by Filipe Apolinário on 10-May-15.
 */
public class FetchAuthorizedForeignWorkspacesCommand extends Command {
    private String foreignEmail;
    private ArrayList<WorkspaceAccess> result;

    public FetchAuthorizedForeignWorkspacesCommand(String foreignEmail) {
        this.foreignEmail = foreignEmail;
    }

    public void execute() {
        FindMyForeignWorkspaceService service = new FindMyForeignWorkspaceService(SimpleApp.getContext());

        service.run(foreignEmail);

        result = service.getResult();
    }

    public ArrayList<WorkspaceAccess> getResult() {
        return result;
    }
}
