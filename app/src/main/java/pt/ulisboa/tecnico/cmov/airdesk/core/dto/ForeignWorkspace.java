package pt.ulisboa.tecnico.cmov.airdesk.core.dto;

/**
 * Created by Filipe Apolinário on 01-May-15.
 */
public class ForeignWorkspace extends DTO {

    private String workspaceName = "";
    private String workspaceOwner = "";

    public ForeignWorkspace(String wsName, String wsOwner) {
        workspaceName = wsName;
        workspaceOwner = wsOwner;
    }

    public String getWorkspaceName() {
        return workspaceName;
    }

    public void setWorkspaceName(String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getWorkspaceOwner() {
        return workspaceOwner;
    }

    public void setWorkspaceOwner(String workspaceOwner) {
        this.workspaceOwner = workspaceOwner;
    }
}
