package pt.ulisboa.tecnico.cmov.airdesk.service.local.file;

/**
 * Created by Group 7 on 04-Apr-15.
 */

import android.content.Context;

import java.io.File;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.FileStorageHandler;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;

/**
 * The Class CreateFileService.
 * This service creates a file in Internal FileStorage.
 */
public class CreateFileService {

    /**
     * The file directory.
     */
    private File fileDirectory;

    /**
     * Instantiates a new creates the file service.
     *
     * @param context the context
     */
    public CreateFileService(Context context) {
        this.fileDirectory = context.getFilesDir();
    }

    /**
     * Run.
     *
     * @param ws       the ws
     * @param fileName the file name
     * @throws ServiceException the service exception
     */
    public void run(Workspace ws, String fileName) throws ServiceException {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {fileName};
        if (!verifier.stringVerifier(sParams) || ws == null) {
            throw new EmptyFieldsException();
        }
        String absolutePath = fileDirectory.getAbsolutePath();
        String wsRelativePath = ws.getDirectory();

        FileStorageHandler handler = new FileStorageHandler();
        String wsAbsPath = absolutePath + File.separator + wsRelativePath;
        if (handler.doesDirectoryExist(wsAbsPath)) {
            handler.createDirectory(wsAbsPath);
        }
        handler.createNewFile(wsAbsPath, fileName);
    }
}
