package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.tasks.remote;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import pt.ulisboa.tecnico.cmov.airdesk.R;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.ForeignWorkspace;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.WorkspaceAccess;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspaceAdapter;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.foreignWorkspaces.ForeignWorkspaceOnlineDialog;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.CommunicationException;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands.workspaceCommands.FetchAuthorizedForeignWorkspacesCommand;
import pt.ulisboa.tecnico.cmov.airdesk.service.remote.services.RequestCommunicationService;

/**
 * The Class FindForeignWorkspaceAsyncTask.
 * Finds the user's Foreign Workspaces
 */
public class FindForeignWorkspaceAsyncTask extends
        AsyncTask<P2PNode, String, ArrayList<WorkspaceAccess>> {

    /**
     * The context.
     */
    private Context context;

    /**
     * The activity.
     */
    private ActionBarActivity activity;

    /**
     * The foreign Email.
     */
    private String foreignEmail;

    private ArrayList<ForeignWorkspace> workspacesList = new ArrayList<>();

    /**
     * Instantiates a new find foreign workspace async task.
     *
     * @param context  the context
     * @param activity the activity
     */
    public FindForeignWorkspaceAsyncTask(Context context, ActionBarActivity activity, String foreignEmail) {
        super();
        this.context = context;
        this.activity = activity;
        this.foreignEmail = foreignEmail;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the array list
     */
    @Override
    protected ArrayList<WorkspaceAccess> doInBackground(P2PNode... params) {
        ArrayList<WorkspaceAccess> workspaceList = new ArrayList<>();
        for (P2PNode peer : params) {
            try {
                FetchAuthorizedForeignWorkspacesCommand command = new FetchAuthorizedForeignWorkspacesCommand(foreignEmail);

                //send request to peer
                publishProgress("Sending request to peer");
                RequestCommunicationService service = new RequestCommunicationService(command, peer, context);
                service.execute();

                publishProgress("Workspaces received");
                FetchAuthorizedForeignWorkspacesCommand response = (FetchAuthorizedForeignWorkspacesCommand) service.getResult();

                workspaceList.addAll(response.getResult());
            } catch (CommunicationException e) {
                publishProgress("Could not perform request");
            }
        }
        return workspaceList;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Toast toast = Toast.makeText(context, values[0], Toast.LENGTH_SHORT);
        toast.show();
    }


    /**
     * On post execute.
     *
     * @param workspaceAccesses the workspace accesses
     */
    @Override
    protected void onPostExecute(ArrayList<WorkspaceAccess> workspaceAccesses) {
        super.onPostExecute(workspaceAccesses);
        for (WorkspaceAccess w : workspaceAccesses) {
            ForeignWorkspace wsA = new ForeignWorkspace(w.getWorkspaceName(), w.getWorkspaceOwnerEmail());
            workspacesList.add(wsA);
        }

        if (workspacesList.isEmpty()) {
            return;
        }
        ForeignWorkspaceAdapter adapter = new ForeignWorkspaceAdapter(activity, workspacesList, activity.getResources());
        final ListView wsListView = (ListView) activity.findViewById(R.id.foreignWsList);
        final AdapterView.OnItemClickListener wsListViewItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                final int position = wsListView.getPositionForView(myView);
                ForeignWorkspace title = (ForeignWorkspace) workspacesList.get(position);
                Bundle args = new Bundle();
                args.putString("title", title.getWorkspaceName());
                args.putString("owner_email", title.getWorkspaceOwner());
                showWorkspaceNoticeDialog(args);
            }
        };
        wsListView.setOnItemClickListener(wsListViewItemClickListener);
        wsListView.setAdapter(adapter);
    }

    /**
     * Show workspace notice dialog.
     *
     * @param args the args
     */
    private void showWorkspaceNoticeDialog(Bundle args) {
        // Create an instance of the dialog fragment and show it
        ForeignWorkspaceOnlineDialog dialog = new ForeignWorkspaceOnlineDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), "NoticeDialogFragment");
    }

}
