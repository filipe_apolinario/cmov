package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.local;

import android.content.Context;
import android.os.AsyncTask;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.BrowseMyWorkspaceActivity;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ServiceException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace.FetchWorkspaceService;

/**
 * Created by Filipe Apolinário on 12-May-15.
 */
public class FetchWorkspaceAsyncTask extends AsyncTask<String, Void, Workspace> {
    BrowseMyWorkspaceActivity activity;
    String name;
    String ownerEmail;
    FetchWorkspaceService service;
    Context context;

    public FetchWorkspaceAsyncTask(BrowseMyWorkspaceActivity activity,
                                   Context context,
                                   String name,
                                   String ownerEmail) {
        this.context = context;
        this.name = name;
        this.activity = activity;
        this.ownerEmail = ownerEmail;
        service = new FetchWorkspaceService(context);
    }

    @Override
    protected Workspace doInBackground(String... params) {
        try {
            service.run(ownerEmail, name);
            return service.getResult();
        } catch (ServiceException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Workspace workspace) {
        super.onPostExecute(workspace);

        if (workspace != null) {
            activity.ws = workspace;
            ListFilesAsyncTask listFilesService = new ListFilesAsyncTask(context,
                    workspace.getDirectory(), workspace.getQuota(), activity);
            listFilesService.execute();
        } else activity.finish();
    }
}
