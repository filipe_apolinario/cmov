package pt.ulisboa.tecnico.cmov.airdesk.service.exception;

/**
 * Created by Group 7 on 08-Apr-15.
 */


/**
 * The Class WorkspaceDoesNotExistException.
 * This Exception is raised when a service tries to perform operations on a Workspace that does not exist.
 */
public class WorkspaceDoesNotExistException extends
        ServiceException {
}
