package pt.ulisboa.tecnico.cmov.airdesk.presentation.workspaces.browseWorkspace.task.local;

/**
 * Created by Group 7 on 06-Apr-15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.ErrorOccurredException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.FileAlreadyExistsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.local.file.CreateFileService;

/**
 * The Class CreateFileAsyncTask.
 * creates the file in FileStorage.
 */
public class CreateFileAsyncTask extends
        AsyncTask<String, Void, Void> {

    /**
     * The service.
     */
    private CreateFileService service;

    /**
     * The dialog.
     */
    private Dialog dialog;

    /**
     * The ws.
     */
    private Workspace ws;

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The activity.
     */
    private Activity activity;

    /**
     * The message.
     */
    private String message;

    /**
     * The context.
     */
    private Context context;

    /**
     * Instantiates a new creates the file async task.
     *
     * @param c        the c
     * @param dialog   the dialog
     * @param activity the activity
     * @param ws       the ws
     * @param fileName the file name
     */
    public CreateFileAsyncTask(Context c,
                               Dialog dialog,
                               Activity activity,
                               Workspace ws,
                               String fileName) {
        super();
        this.service = new CreateFileService(c);
        this.dialog = dialog;
        this.ws = ws;
        this.fileName = fileName;
        this.activity = activity;
        this.context = c;
    }

    /**
     * Do in background.
     *
     * @param params the params
     * @return the void
     */
    @Override
    protected Void doInBackground(String... params) {
        try {
            service.run(ws, fileName);
            this.message = "Successfully created file.";
        } catch (EmptyFieldsException e) {
            this.message = "Some fields were empty.";
        } catch (FileAlreadyExistsException e) {
            this.message = "File already exists.";
        } catch (ErrorOccurredException e) {
            this.message = "An error occurred.";
        }
        return null;
    }

    /**
     * On post execute.
     *
     * @param aVoid the a void
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();

        dialog.dismiss();
        reloadActivity(this.activity);
    }

    /**
     * Reload activity.
     *
     * @param activity the activity
     */
    private void reloadActivity(Activity activity) {
        activity.finish();
        activity.startActivity(activity.getIntent());
    }


}