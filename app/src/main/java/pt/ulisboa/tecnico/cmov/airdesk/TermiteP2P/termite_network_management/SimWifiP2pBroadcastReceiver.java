package pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.inesc.termite.wifidirect.SimWifiP2pDeviceList;
import pt.inesc.termite.wifidirect.SimWifiP2pInfo;
import pt.inesc.termite.wifidirect.SimWifiP2pManager;
import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.communication_tasks.IncomingCommTask;

public class SimWifiP2pBroadcastReceiver extends BroadcastReceiver {

    private Context context;

    public SimWifiP2pBroadcastReceiver(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (SimWifiP2pBroadcast.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            // This action is triggered when the WDSim service changes state:
            // - creating the service generates the WIFI_P2P_STATE_ENABLED event
            // - destroying the service generates the WIFI_P2P_STATE_DISABLED event

            int state = intent.getIntExtra(SimWifiP2pBroadcast.EXTRA_WIFI_STATE, -1);
            if (state == SimWifiP2pBroadcast.WIFI_P2P_STATE_ENABLED) {
                SimpleApp.getWifiDirectStateKeeper().setWifiEnabled(true);
                Toast.makeText(context, "WiFi Direct enabled",
                        Toast.LENGTH_SHORT).show();

                new IncomingCommTask(SimpleApp.getContext()).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                SimpleApp.getWifiDirectStateKeeper().setWifiEnabled(false);
                Toast.makeText(context, "WiFi Direct disabled",
                        Toast.LENGTH_SHORT).show();
            }

        } else if (SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            // Request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            SimWifiP2pDeviceList deviceList = (SimWifiP2pDeviceList) intent.getSerializableExtra(SimWifiP2pBroadcast.EXTRA_DEVICE_LIST);
            SimpleApp.getWifiDirectStateKeeper().setPeerList(deviceList);

            Toast.makeText(context, "Peer list changed\n" + deviceList.getDeviceList().toString(),
                    Toast.LENGTH_SHORT).show();

        } else if (SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION.equals(action)) {

            SimWifiP2pInfo ginfo = (SimWifiP2pInfo) intent.getSerializableExtra(
                    SimWifiP2pBroadcast.EXTRA_GROUP_INFO);


            ginfo.print();
            SimpleApp.getWifiDirectStateKeeper().setDeviceName(ginfo.getDeviceName());
            SimpleApp.getWifiDirectStateKeeper().setSimWifiP2pInfo(ginfo);


            SimpleApp.getWifiDirectStateKeeper().getmManager().requestGroupInfo(SimpleApp.getWifiDirectStateKeeper().getmChannel(), (SimWifiP2pManager.GroupInfoListener) SimpleApp.getWifiDirectStateKeeper());
            Toast.makeText(context, "Network membership changed:\n",
                    Toast.LENGTH_SHORT).show();
            /*
                    //else SimpleApp.simWifiP2pInfo.mergeUpdate(ginfo);
                    //SimpleApp.connectedPeersList = new ArrayList<>();
            for (String name : ginfo.getDevicesInNetwork()) {
                SimWifiP2pDeviceList peerList = SimpleApp.getWifiDirectStateKeeper().getPeerList();
                String virtualIP = peerList.getByName(name).getVirtIp();
                SimpleApp.getWifiDirectStateKeeper().registerConnectedPeer(new P2PNode(name, virtualIP));
            }
            Toast.makeText(context, "Network membership changed:\n" + SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList().toString(),
                    Toast.LENGTH_SHORT).show();
            ArrayList<P2PNode> peerList = SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList();
            String myEmail = null;
            if (SimpleApp.getUserLoggedIn() != null) {
                myEmail = SimpleApp.getUserLoggedIn().getEmail();
            }
            ExchangeEmailCommand command = new ExchangeEmailCommand(myEmail, SimpleApp.getWifiDirectStateKeeper().getDeviceName());

            for (P2PNode peer : peerList) {
                new RequestEmailTask(context, command).executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR, peer);
            }*/

        } else if (SimWifiP2pBroadcast.WIFI_P2P_GROUP_OWNERSHIP_CHANGED_ACTION.equals(action)) {

            SimWifiP2pInfo ginfo = (SimWifiP2pInfo) intent.getSerializableExtra(
                    SimWifiP2pBroadcast.EXTRA_GROUP_INFO);
            ginfo.print();
            SimpleApp.getWifiDirectStateKeeper().setSimWifiP2pInfo(ginfo);
            Toast.makeText(context, "Group ownership changed",
                    Toast.LENGTH_SHORT).show();
            ArrayList<String> devicesInNetwork = new ArrayList<>(ginfo.getDevicesInNetwork());

        }
    }
}
