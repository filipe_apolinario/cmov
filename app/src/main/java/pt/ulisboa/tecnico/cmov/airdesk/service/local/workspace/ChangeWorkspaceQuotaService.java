package pt.ulisboa.tecnico.cmov.airdesk.service.local.workspace;

/**
 * Created by Group 7 on 31-Mar-15.
 */

import android.content.Context;

import pt.ulisboa.tecnico.cmov.airdesk.FieldVerifier;
import pt.ulisboa.tecnico.cmov.airdesk.core.Database.WorkspaceDataSource;
import pt.ulisboa.tecnico.cmov.airdesk.core.dto.Workspace;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.EmptyFieldsException;
import pt.ulisboa.tecnico.cmov.airdesk.service.exception.QuotaExceededException;

/**
 * The Class ChangeWorkspaceQuotaService.
 */
public class ChangeWorkspaceQuotaService {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The database my workspace handler.
     */
    private WorkspaceDataSource databaseMyWorkspaceHandler;

    /**
     * Instantiates a new change workspace quota service.
     *
     * @param context the context
     */
    public ChangeWorkspaceQuotaService(Context context) {
        databaseMyWorkspaceHandler = new WorkspaceDataSource(context);
        this.context = context;
    }

    /**
     * Run.
     *
     * @param name       the name
     * @param ownerEmail the owner email
     * @param newQuota   the new quota
     */
    public void run(String name, String ownerEmail, int newQuota) {
        FieldVerifier verifier = new FieldVerifier();
        String[] sParams = {name, ownerEmail};
        int[] iParams = {newQuota};
        if (!verifier.positiveVerifier(iParams) || !verifier.stringVerifier(sParams)) {
            throw new EmptyFieldsException();
        }

        databaseMyWorkspaceHandler.open();

        Workspace ws = databaseMyWorkspaceHandler.findWorkspaceByNameAndOwner(name, ownerEmail);

        databaseMyWorkspaceHandler.close();

        CheckQuotaService service = new CheckQuotaService(context);
        service.run(ws.getDirectory(), newQuota);

        if (!service.getResult()) {
            throw new QuotaExceededException();
        }

        databaseMyWorkspaceHandler.open();

        ws.setQuota(newQuota);
        databaseMyWorkspaceHandler.changeWorkspace(ws);

        databaseMyWorkspaceHandler.close();
    }
}
