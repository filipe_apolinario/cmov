package pt.ulisboa.tecnico.cmov.airdesk.service.remote.commands;

import pt.ulisboa.tecnico.cmov.airdesk.SimpleApp;
import pt.ulisboa.tecnico.cmov.airdesk.TermiteP2P.termite_network_management.P2PNode;

/**
 * Created by Filipe Apolinário on 11-May-15.
 */
public class ExchangeEmailCommand extends Command {
    private String issuerEmail = "";
    private String issuerDeviceName = "";
    private String executerEmail = "";
    private String executerDeviceName = "";

    public ExchangeEmailCommand(String issuerDeviceName, String issuerEmail) {
        this.issuerDeviceName = issuerDeviceName;
        this.issuerEmail = issuerEmail;
    }

    public String getExecuterEmail() {
        return executerEmail;
    }

    public String getExecuterDeviceName() {
        return executerDeviceName;
    }

    public void execute() {
        for (P2PNode node : SimpleApp.getWifiDirectStateKeeper().getConnectedPeersList()) {
            if (node.getDeviceName().equals(issuerDeviceName)) {
                node.setDeviceName(issuerDeviceName);
                node.setOwnerEmail(issuerEmail);
                break;
            }
        }
        if (SimpleApp.getUserLoggedIn() != null) {
            executerEmail = SimpleApp.getUserLoggedIn().getEmail();
        }
        executerDeviceName = SimpleApp.getWifiDirectStateKeeper().getDeviceName();
    }

}
